app.controller('todoController', ($scope, $timeout, dataService) => {
    $scope.todoList = [];
    $scope.todoItem = '';

    $scope.addItem = () => {
        if ($scope.todoItem.trim()) {
            $scope.todoList.push({ text: $scope.todoItem, isChecked: false, isDeleted: false });
            $scope.todoItem = '';
        }
    }

    $scope.deleteItem = (index) => {
        $scope.todoList[index].isDeleted = true;
        $timeout(() => {
            $scope.todoList.splice(index, 1);
        }, 1000);
    }

    $scope.checkItem = (index) => {
        $scope.todoList[index].isChecked = !$scope.todoList[index].isChecked;
    }

    dataService.getData().then(result => {
        $scope.todoList = result.data;
    })
})